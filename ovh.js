async function ovh_call(method, endpoint, data)
{
	let api_url = 'https://api.ovh.com/1.0'
	let timestamp = await fetch('https://api.ovh.com/1.0/auth/time').then(response => response.text())
	let app_key = document.getElementById('app_key').value
	let secret_key = document.getElementById('secret_key').value
	let consumer_key = document.getElementById('consumer_key').value
	let json_data = data ? JSON.stringify(data) : ''
	let signature = secret_key + '+' + consumer_key + '+' + method + '+' + api_url + endpoint + '+' + json_data + '+' + timestamp
	let sigkey = '$1$' + sha1(signature)

	return fetch(api_url + endpoint,
		{
			method: method,
			headers:
			{
				'Accept': 'application/json',
				'Content-Type': 'application/json;charset=utf-8',
				'X-Ovh-Application': app_key,
				'X-Ovh-Consumer': consumer_key,
				'X-Ovh-Timestamp': await fetch('https://api.ovh.com/1.0/auth/time').then(response => response.text()),
				'X-Ovh-Signature': sigkey,
			},
			body: json_data ? json_data : undefined
		})
		.then(response => 
		{
			if (response.ok)
				return response.json()
			else if (response.status == 403)
			{
				bulmaToast.toast({ message: 'Action non autorisée par OVH. Vérifiez app, secret et consumer key', type: 'is-danger' })
				return false
			}
		})
}


async function ovh_create_credentials()
{
	fetch('https://api.ovh.com/1.0/auth/credential',
		{
			method: 'POST',
			headers:
			{
				'Accept': 'application/json',
				'Content-Type': 'application/json;charset=utf-8',
				'X-Ovh-Application': document.getElementById('app_key').value,
				'X-Ovh-Timestamp': await fetch('https://api.ovh.com/1.0/auth/time').then(response => response.text()),
			},
			body: JSON.stringify(
				{
					"accessRules": [
						{
							"method": "DELETE",
							"path": "*"
						},
						{
							"method": "GET",
							"path": "*"
						},
						{
							"method": "POST",
							"path": "*"
						},
						{
							"method": "PUT",
							"path": "*"
						}
					],
					"redirection": "https://installer.optimus-avocats.fr"
				}
			)
		})
		.then(response => response.json())
		.then(response =>
		{
			localStorage.setItem('consumer_key', response.consumerKey)
			if (response.consumerKey.length == 32)
				document.getElementById('continue3').classList.remove('is-hidden')
			window.open(response.validationUrl)
		})
}

async function ovh_vps_info()
{
	document.querySelectorAll('#vps_info > span').forEach(span => span.innerText = '')

	ovh_call('GET', '/vps/' + document.getElementById('vps_select').value, '')
		.then(response => 
		{
			document.getElementById('vps_name').innerText = response?.displayName
			document.getElementById('vps_model_vcore').innerText = response.model.vcore
			document.getElementById('vps_model_memory').innerText = Math.round(response.model.memory / 1024) + ' Go'
			document.getElementById('vps_model_disk').innerText = response.model.disk + ' Go'
			document.getElementById('vps_model_version').innerText = response.model.offer + ' (' + response.model.version + ')'
		})

	ovh_call('GET', '/vps/' + document.getElementById('vps_select').value + '/serviceInfos', '')
		.then(response => 
		{
			document.getElementById('vps_creation').innerText = response?.creation
		})

	ovh_call('GET', '/vps/' + document.getElementById('vps_select').value + '/images/current', '')
		.then(response => 
		{
			document.getElementById('vps_system').innerText = response?.name
		})
}

async function ovh_vps_rebuild()
{
	document.getElementById('vps_rebuild_progress_block').classList.remove('is-hidden')
	document.getElementById('vps_rebuild_button').classList.add('is-loading')
	document.getElementById('vps_rebuild_percentage').innerText = '0 %'
	let rebuild = await ovh_call('POST', '/vps/' + localStorage.getItem('vps') + '/rebuild',
		{
			"doNotSendPassword": true,
			"imageId": await vps_find_debian12_image_id(),
			"installRTM": true,
			"publicSshKey": "ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBHi9xy8P/Z2ZCxmqIwijW4AMnpZgRNCPPYp5dNk3uLuxn2OsUpYNwUOcox2eGaRIAIpPKgo2b4pcnZWSrz6r/FA= installer@cybertron.fr"
		})

	let vps_progress = setInterval(() =>
	{
		ovh_call('GET', '/vps/' + localStorage.getItem('vps') + '/tasks/' + rebuild.id, '')
			.then(response =>
			{
				document.getElementById('vps_rebuild_progress').value = response.progress
				document.getElementById('vps_rebuild_percentage').innerText = response.progress + ' %'
				if (response.progress == 100)
				{
					document.getElementById('vps_rebuild_button').classList.remove('is-loading')
					document.getElementById('continue6').classList.remove('is-hidden')
					clearInterval(vps_progress)
				}
			})
	}, 2000)
	return rebuild
}

async function vps_ssh_connect()
{
	await socket.send(JSON.stringify(
		{
			"command": "ssh_connect",
			"data":
			{
				"vps": localStorage.getItem('vps'),
				"ovh_secret": localStorage.getItem('secret_key'),
				"ovh_app": localStorage.getItem('app_key'),
				"ovh_consumer": localStorage.getItem('consumer_key'),
			}
		}))
}

async function vps_ssh_command(command, data)
{
	if (command != 'status')
	{
		document.getElementById('output_container').classList.remove('is-hidden')
		document.getElementById('progress').value = 0
		document.getElementById('output').innerHTML = ''
	}
	socket.send(JSON.stringify({
		command: command,
		data: data
	}))
}

async function vps_find_debian12_image_id()
{
	let image_ids = await ovh_call("GET", "/vps/" + localStorage.getItem('vps') + "/images/available")
	let step = 35 / image_ids.length
	console.log(step)
	for (let image_id of image_ids)
	{
		document.getElementById('vps_rebuild_progress').value += step
		document.getElementById('vps_rebuild_percentage').innerText = Math.round(document.getElementById('vps_rebuild_progress').value) + ' %'
		let image = await ovh_call("GET", "/vps/" + localStorage.getItem('vps') + "/images/available/" + image_id)
		if (image?.name == "Debian 12")
			return image.id
	}
	bulmaToast.toast({ message: "L'API OVH est momentanément indisponible. Ré-essayez dans quelques secondes", type: 'is-warning' })
	document.getElementById('vps_rebuild_button').classList.remove('is-loading')
}