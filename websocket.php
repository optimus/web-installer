<?php
$websocket_server_ip = '0.0.0.0';
$websocket_server_port = 50001;
$websocket_server_local_cert = '/home/metallian/fullchain.pem';
$websocket_server_local_pk = '/home/metallian/privkey.pem';
$ssh_private_key = '/home/metallian/optimus-installer-ecdsa';
$ssh_public_key = '/home/metallian/optimus-installer-ecdsa.pub';
$logfile = '/home/metallian/optimus-installer.log';

function ovh_api_request($type, $endpoint, $data = '', $ovh_secret, $ovh_app, $ovh_consumer)
{
	if (!$ovh_secret OR !$ovh_app OR !$ovh_consumer)
		return json_decode('{"code":"400", "message":"OVH credentials missing"}');
	$api_url = 'https://api.ovh.com/1.0';
	$timestamp = file_get_contents("https://api.ovh.com/1.0/auth/time");
	$signature = $ovh_secret . '+' . $ovh_consumer . '+' . $type . '+' . $api_url . $endpoint . '+' . $data . '+' . $timestamp;
	$sigkey = '$1$' . sha1($signature);

	$curl = curl_init();
	curl_setopt($curl, CURLOPT_URL, $api_url . $endpoint);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_POST, 1);
	curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $type);
	curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
	curl_setopt(
		$curl,
		CURLOPT_HTTPHEADER,
		[
			'Content-Type:application/json;charset=utf-8',
			'X-Ovh-Application:' . $ovh_app,
			'X-Ovh-Consumer:' . $ovh_consumer,
			'X-Ovh-Timestamp:' . $timestamp,
			'X-Ovh-Signature:' . $sigkey
		]
	);
	$response = curl_exec($curl);
	curl_close($curl);
	return json_decode($response);
}

function nomask($payload, $type = 'text') 
{
	$frameHead = array();
	$frame = '';
	$payloadLength = strlen($payload);

	if ($type == 'text')
		$frameHead[0] = 129;
	else if ($type == 'close')
		$frameHead[0] = 136;
	else if ($type == 'ping')
		$frameHead[0] = 137;
	else if ($type == 'pong')
		$frameHead[0] = 138;

	if ($payloadLength > 65535) 
	{
		$payloadLengthBin = str_split(sprintf('%064b', $payloadLength), 8);
		$frameHead[1] = 127;
		for ($i = 0; $i < 8; $i++)
			$frameHead[$i + 2] = bindec($payloadLengthBin[$i]);

		if ($frameHead[2] > 127) 
		{
			$this->close(1004);
			return false;
		}
	}
	elseif ($payloadLength > 125) 
	{
		$payloadLengthBin = str_split(sprintf('%016b', $payloadLength), 8);
		$frameHead[1] = 126;
		$frameHead[2] = bindec($payloadLengthBin[0]);
		$frameHead[3] = bindec($payloadLengthBin[1]);
	}
	else 
		$frameHead[1] = $payloadLength;

	foreach (array_keys($frameHead) as $i)
		$frameHead[$i] = chr($frameHead[$i]);

	$frame = implode('', $frameHead);
	$frame .= $payload;
	return $frame;
}

function handshake($client)
{
	$headers_stream = fread($client, 1500);
	$headers = array();
	$lines = preg_split("/\r\n/", $headers_stream);
	foreach($lines as $line)
	{
		$line = rtrim($line);
		if(preg_match('/\A(\S+): (.*)\z/', $line, $matches))
			$headers[$matches[1]] = $matches[2];
	}

	$ip_and_port = stream_socket_get_name($client, true);

	$user = array
	(
		"ip" => explode(':', $ip_and_port)[0], 
		"port" => explode(':', $ip_and_port)[1],
		"socket" => $client,
		"requests" => 0,
		"connection_time" => microtime(),
		"stream" => '',
		"ssh_connection" => null,
		"ssh_stream" => null,
		"ssh_error_stream" => null,
	);

	$secKey = $headers['Sec-WebSocket-Key'];
	$secAccept = base64_encode(pack('H*', sha1($secKey . '258EAFA5-E914-47DA-95CA-C5AB0DC85B11')));
	$upgrade  = "HTTP/1.1 101 Web Socket Protocol Handshake\r\n" .
	"Upgrade: websocket\r\n" .
	"Connection: Upgrade\r\n" .
	"WebSocket-Origin: " . $websocket_server_ip . "\r\n" .
	"WebSocket-Location: wss://" . $websocket_server_ip . ":" . $websocket_server_port . "\r\n".
	"Sec-WebSocket-Version: 13\r\n" .
	"Sec-WebSocket-Accept:" . $secAccept . "\r\n\r\n";
	fwrite($user['socket'], $upgrade);

	return $user;
}

function disconnect($user, $code, $reason)
{
	global $users;
	error_log("Websocket connection closed with" . $user['ip'] . ":" . $user['port'] . " (" . $reason .")");
	if (is_resource($user['socket']))
	{
		fwrite($user['socket'], nomask(pack('n*', $code) . $reason, $type = 'close', false));
		fclose($user['socket']);
	}
	for($i = 0; $i < sizeof($users); $i++)
		if ($users[$i]['ip'] == $user['ip'] AND $users[$i]['port'] == $user['port'])
			array_splice($users, $i, 1);
}

function ssh_stream(&$user, $command)
{
	if (!$user['ssh_connection'])
		fwrite($user['socket'], nomask('{"code":400, "message":"you must ssh_connect first"}'));
	if (@$user['ssh_stream'])
		fwrite($user['socket'], nomask('{"code":400, "message":"another script is already running"}'));
	else
	{
		$ssh_stream = ssh2_exec($user['ssh_connection'], $command);
		if ($ssh_stream)
		{
			$ssh_error_stream = ssh2_fetch_stream($ssh_stream, SSH2_STREAM_STDERR);
			stream_set_blocking($ssh_stream, true);
			stream_set_blocking($ssh_error_stream, true);
			$user['ssh_stream'] = $ssh_stream;
			$user['ssh_error_stream'] = $ssh_error_stream;
		}
		else
			fwrite($user['socket'], nomask('{"code":403, "message":"SSH session disconnected"}'));
	}
}



ini_set('default_socket_timeout', 2);
$context = stream_context_create();
$null = null;
$users = array();
stream_context_set_option($context, 'ssl', 'local_cert', $websocket_server_local_cert);
stream_context_set_option($context, 'ssl', 'local_pk', $websocket_server_local_pk);
stream_context_set_option($context, 'ssl', 'allow_self_signed', true);
stream_context_set_option($context, 'ssl', 'verify_peer', false);
$server = stream_socket_server("tcp://{$websocket_server_ip}:{$websocket_server_port}", $errno, $errstr, STREAM_SERVER_BIND|STREAM_SERVER_LISTEN, $context);
if (!$server)
	echo("$errstr ($errno)");

$clients = array($server);
$users = array();
$last_second = time();

echo("Listening to {$websocket_server_ip}:{$websocket_server_port} for connections\n");

while(true)
{
	$changed = array($server);
	foreach($users as $user)
		array_push($changed, $user['socket']);

	if (stream_select($changed, $null, $null, 0, 10) > 0)
	{
		if (in_array($server, $changed)) 
		{
			$client = @stream_socket_accept($server);
			if (!$client)
				continue;

			stream_set_blocking($client, true);
			stream_socket_enable_crypto($client, true, STREAM_CRYPTO_METHOD_SSLv23_SERVER);
			$user = handshake($client);
			array_push($users, $user);
			stream_set_blocking($client, false);

			fwrite($client, nomask('{"code":200, "message":"Websocket connected successfully"}'));
			
			error_log("Websocket connection from " . $user['ip'] . ":" . $user['port']);

			$found_socket = array_search($server, $changed);
			unset($changed[$found_socket]);
		}

		foreach ($changed as $changed_socket) 
		{
			for ($i=0; $i<sizeof($users); $i++)
				if ($users[$i]['socket'] == $changed_socket)
					$user_index = $i;

			$current_user = &$users[$user_index];

			//FLOOD PROTECTION (MAX RATE)
			$users[$user_index]['requests']++;
			if ($users[$user_index]['requests'] > 100)
			{
				disconnect($users[$user_index], 1008, "Flood protection : Too much requests (" . $users[$user_index]['requests'] . ")");
				continue;
			}

			//PROCESS INCOMING DATA
			if ($current_user['socket'])
				$current_user['stream'] .= fread($current_user['socket'], 1048576);
			else
				disconnect($current_user, 1000, 'Connection lost');

			if (strlen($current_user['stream']) == 0)
				disconnect($current_user, 1007, "Stream with no data");

			//FLOOD PROTECTION (STREAM SIZE)
			if (strlen($current_user['stream']) == 8192)
			{
				disconnect($current_user, 1009, "Flood protection : Too large payload (max 8192 bytes)");
				continue;
			}

			$stream = $current_user['stream'];
			while ($stream !== '')
			{
				$users[$user_index]['requests']++;	
				if ($users[$user_index]['requests'] > 100)
				{
					disconnect($users[$user_index], 1008, "Flood protection : Too much requests (" . $users[$user_index]['requests'] . ")");
					$stream = '';
					continue;
				}

				unset($opcode);
				unset($payload);

				$opcode = @bindec(substr(sprintf('%08b', ord($stream[0])), 4, 4));
				$payload_length  = ord($stream[1]) & 127;

				$mask_offset = 2;
				$payload_offset = 6;
					
				if($payload_length == 126)
				{
					$mask_offset = 4;
					$payload_offset = 8;
					$payload_length = (ord($stream[2]) << 8) + ord($stream[3]);
				}
				else if($payload_length == 127)
				{
					$mask_offset = 10;
					$payload_offset = 14;
					$payload_length  = (ord($stream[2]) << 56) + (ord($stream[3]) << 48) + (ord($stream[4]) << 40) + (ord($stream[5]) << 32) + (ord($stream[6]) << 24) +(ord($stream[7]) << 16) + (ord($stream[8]) << 8) + ord($stream[9]);
				}

				$mask = substr($stream, $mask_offset, 4);
				$encoded_payload = substr($stream, $payload_offset, $payload_length);

				for($i = 0; $i < strlen($encoded_payload); $i++)
					@$payload .= $encoded_payload[$i] ^ $mask[$i % 4];

				$stream = substr($stream, $payload_offset + $payload_length);

				//VALID COMMAND RECEIVED
				if ($opcode == 1)
				{
					$input = json_decode($payload);
					if (json_last_error())
					{
						fwrite($changed_socket, nomask('{"code":400, "message":"invalid json"}'));
						continue;
					}
					echo "\nMessage from " . $current_user['ip'] . " : \n";
					print_r($input);
					
					//SSH CONNECTION
					if ($input->command == 'ssh_connect')
					{
						$connection = @ssh2_connect($input->data->vps, 22);
						
						if(!$connection)
							$connection = @ssh2_connect($input->data->vps, 7822);
						
						if(!$connection)
							fwrite($changed_socket, nomask('{"code":400, "message":"SSH connection failed"}'));
						
						if($connection)
						{
							fwrite($changed_socket, nomask('{"code":200, "message":"SSH connected successfully"}'));
							$auth  = ssh2_auth_pubkey_file($connection, 'debian', $ssh_public_key, $ssh_private_key);
							if(!$auth)
								fwrite($changed_socket, nomask('{"code":403, "message":"SSH authentication failed"}'));
							else
							{
								$ovh_validator = ovh_api_request('GET', '/vps/' . $input->data->vps, $data = '', $input->data->ovh_secret, $input->data->ovh_app, $input->data->ovh_consumer);
								if (@$ovh_validator->name == $input->data->vps)
								{
									fwrite($changed_socket, nomask('{"code":200, "message":"SSH authenticated successfully"}'));
									$current_user['ssh_connection'] = $connection;
								}
								else
									fwrite($changed_socket, nomask('{"code":403, "message":"OVH verification failed", "reason":"' . $ovh_validator->message . '"}'));
							}
						}
					}
					//OTHER COMMANDS
					else if ($input->command == 'optimus-installer')
						ssh_stream($current_user, "
						sudo wget -O /tmp/install.sh https://git.cybertron.fr/optimus/optimus-installer/-/raw/main/install.sh;
						sudo bash /tmp/install.sh --mode=json --generate --domain=" . $input->data->domain . " --app-key=" . $input->data->app_key . " --consumer-key=" . $input->data->consumer_key . " --secret-key=" . $input->data->secret_key . ";
						sudo rm /tmp/install.sh");
					else if ($input->command == 'remove_cybertron_installer_key')
						ssh_stream($current_user, "sed -i '/ *installer@cybertron.fr *$/d' /home/debian/.ssh/authorized_keys");
					else if ($input->command == 'status')
						ssh_stream($current_user, "sudo bash /etc/optimus/status.sh json");
					else if ($input->command == 'upgrade')
						ssh_stream($current_user, "sudo bash /etc/optimus/upgrade/install.sh json");
					else if ($input->command == 'diskpart')
						ssh_stream($current_user, "sudo bash /etc/optimus/diskpart/install.sh json");
					else if ($input->command == 'crypt')
						ssh_stream($current_user, "sudo bash /etc/optimus/crypt/install.sh json");
					else if ($input->command == 'letsencrypt')
						ssh_stream($current_user, "sudo bash /etc/optimus/letsencrypt/install.sh json");
					else if ($input->command == 'nginx')
						ssh_stream($current_user, "sudo bash /etc/optimus/nginx/install.sh json");
					else if ($input->command == 'docker')
						ssh_stream($current_user, "sudo bash /etc/optimus/docker/install.sh json");
					else if ($input->command == 'firewall')
						ssh_stream($current_user, "sudo bash /etc/optimus/firewall/install.sh json");
					else if ($input->command == 'fail2ban')
						ssh_stream($current_user, "sudo bash /etc/optimus/fail2ban/install.sh json");
					else if ($input->command == 'ssh_port_change')
						ssh_stream($current_user, "sudo bash /etc/optimus/ssh_port_change/install.sh json");
					else if ($input->command == '2fa')
						ssh_stream($current_user, "sudo bash /etc/optimus/2fa/install.sh json");
					else if ($input->command == 'debian_password')
						ssh_stream($current_user, "sudo bash /etc/optimus/debian_password/install.sh json");
					else if ($input->command == 'optimus-init')
						ssh_stream($current_user, "sudo bash /etc/optimus/optimus-init/install.sh json");
					else if ($input->command == 'optimus-databases')
						ssh_stream($current_user, "sudo bash /etc/optimus/optimus-databases/install.sh json");
					else if ($input->command == 'optimus-base')
						ssh_stream($current_user, "sudo bash /etc/optimus/optimus-base/install.sh json");
					else if ($input->command == 'cybertron_ssh_key')
						ssh_stream($current_user, "sudo bash /etc/optimus/cybertron_ssh_key/install.sh json");
					else if ($input->command == 'port_knocking')
						ssh_stream($current_user, "sudo bash /etc/optimus/port_knocking/install.sh json");
					else if ($input->command == 'create_admin')
					{
						if (!isset($input->data->domain) || $input->data->domain == '')
							fwrite($changed_socket, nomask('{"code":400, "operation":"create_admin", "error": "Aucun domaine n\'a été renseigné"}'));
						else if (!isset($input->data->admin_email) || $input->data->admin_email == '')
							fwrite($changed_socket,nomask('{"code":400, "operation":"create_admin", "error": "Aucun identifiant n\'a été renseigné"}'));
						else if (!isset($input->data->admin_password) || $input->data->admin_password == '')
							fwrite($changed_socket, nomask('{"code":400, "operation":"create_admin", "error": "Aucun mot de passe n\'a été renseigné"}'));
						else if (!preg_match("/^(([a-z0-9][a-z0-9-_.]*[a-z0-9])+@([a-z0-9][a-z0-9-]*[a-z0-9]\.)+([a-z0-9][a-z0-9-]*[a-z0-9]))$/", $input->data->admin_email))
							fwrite($changed_socket, nomask('{"code":400, "operation":"create_admin", "error": "' . $input->data->admin_email . ' n\'est pas une adresse email valide"}'));
						else if (substr($input->data->admin_email, -(strlen($input->data->domain)+1)) != '@'.$input->data->domain)
							fwrite($changed_socket, nomask('{"code":400, "operation":"create_admin", "error": "L\'adresse mail doit terminer par @' . $input->data->domain . '"}'));
						else if (!preg_match('@[A-Z]@', $input->data->admin_password))
							fwrite($changed_socket, nomask('{"code":400, "operation":"create_admin", "error": "Le mot de passe doit contenir au moins une lettre majuscule"}'));
						else if (!preg_match('@[a-z]@', $input->data->admin_password)) 
							fwrite($changed_socket, nomask('{"code":400, "operation":"create_admin", "error": "Le mot de passe doit contenir au moins une lettre minuscule"}'));
						else if (!preg_match('@[0-9]@', $input->data->admin_password)) 
							fwrite($changed_socket, nomask('{"code":400, "operation":"create_admin", "error": "Le mot de passe doit contenir au moins un chiffre"}'));
						else if (strlen($input->data->admin_password) < 12) 
							fwrite($changed_socket, nomask('{"code":400, "operation":"create_admin", "error": "Le mot de passe doit contenir au moins 12 caractères"}'));
						else if (strlen($input->data->admin_password) > 64) 
							fwrite($changed_socket, nomask('{"code":400, "operation":"create_admin", "error": "Le mot de passe doit contenir au maximum 64 caractères"}'));
						else
							ssh_stream($current_user, "sudo bash /etc/optimus/create_admin/install.sh json " . $input->data->domain . " ". $input->data->admin_email . " " . $input->data->admin_password);
						if ($input->data->domain != 'testoptimu=s.ovh' AND $input->data->domain != 'vestoptimus.ovh')
							file_put_contents($logfile, date('Y-m-d H:i:s') . ' --> ' . $input->data->domain . "\n", FILE_APPEND);
					}
					else if ($input->command == 'get_config')
						ssh_stream($current_user, "sudo bash /etc/optimus/get_config/install.sh json");
					else
						fwrite($changed_socket, nomask('{"code":404, "message":"commande inconnue"}'));
				}
				else if ($opcode == 8)
					disconnect($current_user, 1008, "Graceful disconnection");
			}

			$current_user['stream'] = $stream;
		}
	}

	if (time() > $last_second + 1)
	{
		for ($i=0; $i < sizeof($users); $i++)
			$users[$i]['requests'] = max($users[$i]['requests'] - 20, 0);
		$last_second = time();
	}

	for ($i=0; $i < sizeof($users); $i++)
	{
		if (@$users[$i]['ssh_stream'])
		{
			$line = @fgets($users[$i]['ssh_stream']);
			if ($line)
			{
				$line = preg_replace('#\\x1b[[][^A-Za-z]*[A-Za-z]#', '', $line);
				$line = preg_replace('/[\x00-\x1F]/', '', $line);
				json_decode($line);
				if (json_last_error())
					$line = '{"code":200, "message":"' .$line . '"}';
				echo $line . "\n";
				fwrite($users[$i]['socket'], nomask($line));
			}
			else
				$users[$i]['ssh_stream'] = "";
		}
		if (@$users[$i]['ssh_error_stream'])
		{
			$line = fgets($users[$i]['ssh_error_stream']);
			if ($line)
			{
				json_decode($line);
				$line = preg_replace('#\\x1b[[][^A-Za-z]*[A-Za-z]#', '', $line);
				$line = preg_replace('/[\x00-\x1F]/', '', $line);
				if (json_last_error())
					$line = '{"code":200, "error":"' .$line . '"}';
				echo $line . "\n";
				fwrite($users[$i]['socket'], nomask($line));
			}
			else
				$users[$i]['ssh_error_stream'] = "";
		}
	}	
	
	usleep(1000);
}
?>