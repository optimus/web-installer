let categories = [
	{ id: 1, description: 'Configuration Initiale (obligatoire)' },
	{ id: 2, description: 'Chiffrement du disque (fortement recommandé)' },
	{ id: 3, description: 'Installation des logiciels (obligatoire)' },
	{ id: 4, description: 'Installation des services Optimus (obligatoire)' },
	{ id: 5, description: 'Sécurisation du serveur (fortement recommandé)' },
	{ id: 6, description: 'Options (facultatives)' },
]

let modules = [
	{ id: "optimus-installer", description: 'Installeur Optimus', command: 'optimus-installer', parameters: { domain: localStorage.getItem('domain'), app_key: localStorage.getItem('app_key'), consumer_key: localStorage.getItem('consumer_key'), secret_key: localStorage.getItem('secret_key') }, categorie: 1, required: 1, autoinstall: true },
	{ id: "upgrade", description: 'Mise à jour de Debian', command: 'upgrade', categorie: 1, required: 1, autoinstall: true },
	{ id: "diskpart", description: 'Diviser le disque', command: 'diskpart', categorie: 2, required: 2, autoinstall: true },
	{ id: "crypt", description: 'Chiffrer le disque de données', command: 'crypt', categorie: 2, required: 2, autoinstall: true },
	{ id: "letsencrypt", description: 'Installer les certificats SSL', command: 'letsencrypt', categorie: 3, required: 1, autoinstall: true },
	{ id: "nginx", description: 'Installer NGINX', command: 'nginx', categorie: 3, required: 1, autoinstall: true },
	{ id: "docker", description: 'Installer DOCKER', command: 'docker', categorie: 3, required: 1, autoinstall: true },
	{ id: "optimus-databases", description: 'Optimus Databases', command: 'optimus-databases', categorie: 4, required: 1, autoinstall: true },
	{ id: "optimus-init", description: 'Préparation du serveur', command: 'optimus-init', categorie: 4, required: 1, autoinstall: true },
	{ id: "optimus-base", description: 'Optimus Base', command: 'optimus-base', categorie: 4, required: 1, autoinstall: true },
	{ id: "firewall", description: 'Pare Feu', command: 'firewall', categorie: 5, required: 2, autoinstall: true },
	{ id: "fail2ban", description: 'Fail2Ban', command: 'fail2ban', categorie: 5, required: 2, autoinstall: true },
	{ id: "ssh_port_change", description: 'Remplacer le port SSH par 7822', command: 'ssh_port_change', categorie: 5, required: 2, autoinstall: true },
	{ id: "debian_password", description: 'Modifier le mot de passe debian', command: 'debian_password', categorie: 5, required: 2, autoinstall: true },
	{ id: "2fa", description: 'Activer le 2FA', command: '2fa', categorie: 5, required: 2, autoinstall: true },
	{ id: "cybertron_ssh_key", description: 'Accès SSH CYBERTRON', command: 'cybertron_ssh_key', categorie: 6, required: 3, autoinstall: false, tooltip_class: 'has-tooltip-warning', tooltip_text: "Cette option permet au service support de l'association CYBERTRON d'accéder à votre serveur (et à vos données !) pour vous dépanner en cas de problème." },
	{ id: "port_knocking", description: 'Port knocking', command: 'port_knocking', categorie: 6, required: 3, autoinstall: false, tooltip_class: 'has-tooltip-warning', tooltip_text: "Réservé aux experts : cette option augmente la sécurité mais rend la connexion au serveur SSH plus difficile." },
]