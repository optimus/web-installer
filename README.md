# web-installer

Interface web d'installation facile
Fonctionne avec le module  optimus-installer qui contient les scripts d'installation

La communication avec le serveur se fait par une interface websocket qui permet de vérifier les différentes étapes du processus d'installation

Les écrans d'installation sont décrits dans les fichiers step0.html à step10.html


# Installer ssh2 en PECL dans PHP si le module ne peut pas être activé autrement
```
sudo apt install php-dev
sudo apt-get install -y --no-install-recommends libssh2-1-dev
sudo pecl install ssh2
```

# Générer une paire clé privée / clé publique compatible avec la fonction ssh2_auth_pubkey_file de php8
```
sudo ssh-keygen -m PEM -t ecdsa-sha2-nistp256 -f optimus-installer-ecdsa
```

Veillez à bien configurer les droits d'accès aux fichiers générés

```
sudo chmod 400 optimus-installer-ecdsa
sudo chmod 400 optimus-installer-ecdsa.pub
sudo chown www-data:www-data optimus-installer-ecdsa.pub
sudo chown www-data:www-data optimus-installer-ecdsa.pub
```

# Ouvrir le port 50001 pour accepter les connexionx
```
sudo ufw allow 50001
```

# Lancer le serveur websocket
```
sudo -u www-data php /var/www/installer.optimus-avocats.fr/websocket.php
```

# Lancer le serveur websocket en arrière plan
```
sudo -u www-data nohup php /var/www/installer.optimus-avocats.fr/websocket.php >/dev/null 2>&1 &
```