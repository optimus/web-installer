const query = new URLSearchParams(window.location.search)
let step = 0

bulmaToast.setDefaults({
	message: '',
	type: 'is-primary',
	position: 'bottom-right',
	dismissible: true,
	duration: 4000,
	pauseOnHover: true,
	animate: { in: 'fadeIn', out: 'fadeOut' }
})


async function load(id, dontvalidate)
{
	document.querySelectorAll('#container > div').forEach(step => step.classList.add('is-hidden'))

	if (!document.getElementById('content' + id))
	{
		await fetch('step' + id + '.html')
			.then(response => response.text())
			.then(response =>
			{
				let div = document.createElement('div')
				div.id = 'content' + id
				div.classList.add('is-hidden')
				div.innerHTML = response
				document.querySelector('#container').appendChild(div)
			})
	}

	document.querySelectorAll('.steps > li').forEach(step =>
	{
		step.classList.toggle('is-active', step.id == 'button' + id)
		if (step.id.replace('button', '') <= id)
		{
			step.classList.add('is-clickable')
			step.addEventListener('click', function () { load(this.id.replace('button', ''), true) })
		}
	})

	document.getElementById('content' + id).classList.remove('is-hidden')

	await run(id)
	step = id
	if (!dontvalidate)
		return await validate(id)
}


async function run(id)
{
	if (id == 2)
	{
		if (localStorage.getItem('app_key')?.length == 16)
			document.getElementById('app_key').value = localStorage.getItem('app_key')
		if (localStorage?.getItem('secret_key')?.length == 32)
			document.getElementById('secret_key').value = localStorage.getItem('secret_key')
		if (localStorage?.getItem('app_key')?.length == 16 && localStorage.getItem('secret_key').length == 32)
			document.getElementById('continue2').classList.remove('is-hidden')
		else
			return false
	}
	else if (id == 3)
	{
		if (localStorage.getItem('consumer_key')?.length == 32)
		{
			document.getElementById('consumer_key').value = localStorage.getItem('consumer_key')
			document.getElementById('consumer_key_block').classList.remove('is-hidden')
			document.getElementById('continue3').classList.remove('is-hidden')
		}
		else
			return false
	}
	else if (id == 4)
	{
		document.getElementById('reload4').classList.add('is-loading')
		let domains = await ovh_call('GET', '/domain', '')
		document.getElementById('reload4').classList.remove('is-loading')
		if (domains != false && domains.length > 0)
		{
			domains.forEach(domain => document.getElementById('domain_select').options.add(new Option(domain, domain)))
			document.getElementById('no_domain_info_block').classList.add('is-hidden')
			document.getElementById('domain_selector_block').classList.remove('is-hidden')
			if (localStorage.getItem('domain'))
				document.getElementById('domain_select').value = localStorage.getItem('domain')
		}
		else
		{
			document.getElementById('domain_selector_block').classList.add('is-hidden')
			document.getElementById('no_domain_info_block').classList.remove('is-hidden')
			return false
		}
	}
	else if (id == 5)
	{
		document.getElementById('reload5').classList.add('is-loading')
		let vps = await ovh_call('GET', '/vps', '')
		document.getElementById('reload5').classList.remove('is-loading')
		if (vps != false && vps.length > 0)
		{
			vps.forEach(vp => document.getElementById('vps_select').options.add(new Option(vp, vp)))
			document.getElementById('no_vps_info_block').classList.add('is-hidden')
			document.getElementById('vps_selector_block').classList.remove('is-hidden')
			if (localStorage.getItem('vps'))
			{
				document.getElementById('vps_select').value = localStorage.getItem('vps')
				ovh_vps_info()
			}
			else
			{
				ovh_vps_info()
				return false
			}
		}
		else
		{
			document.getElementById('vps_selector_block').classList.add('is-hidden')
			document.getElementById('no_vps_info_block').classList.remove('is-hidden')
			return false
		}
	}
	else if (id == 6)
	{
		if (localStorage.getItem('vps'))
			document.getElementById('server_id').innerText = localStorage.getItem('vps')
	}
	else if (id == 7)
	{
		if (!window?.socket)
		{
			socket = new WebSocket("wss://installer.optimus-avocats.fr:50001")
			socket.addEventListener("message", event =>
			{
				if (!event.data)
					return false
				output = JSON.parse(event.data)
				if (output.message == 'Websocket connected successfully')
					vps_ssh_connect()
				else if (output.message == 'OVH verification failed')
					bulmaToast.toast({ message: 'La connexion au serveur a échoué<br/>' + output?.reason, type: 'is-danger' })
				else if (output.message == 'SSH authenticated successfully')
				{
					bulmaToast.toast({ message: 'La connexion au serveur a été établie', type: 'is-success' })
					document.getElementById('module_container').innerHTML = ''
					document.getElementById('continue7').classList.add('is-hidden')
					for (categorie of categories)
					{
						let subtitle = document.createElement('div')
						subtitle.classList.add('subtitle')
						subtitle.classList.add('mb-3')
						subtitle.innerText = categorie.description
						document.getElementById('module_container').appendChild(subtitle)

						let buttons = document.createElement('div')
						buttons.classList.add('buttons')
						document.getElementById('module_container').appendChild(buttons)

						for (let module of modules)
							if (module.categorie == categorie.id)
							{
								let button = document.createElement('button')
								button.id = module.id + '_button'
								if (module.required == 1)
									button.classList.add('is-danger')
								else if (module.required == 2)
									button.classList.add('is-warning')
								button.classList.add('button')
								button.classList.add('is-outlined')
								if (module.required != 3)
									button.classList.add('is-light')
								button.onclick = function ()
								{
									localStorage.setItem('installation_type', 'manual')
									vps_ssh_command(module.command, module.parameters)
								}
								button.innerText = module.description

								if (module.tooltip_text)
								{
									button.classList.add('has-tooltip-multiline')
									button.classList.add(module.tooltip_class)
									button.dataset.tooltip = module.tooltip_text
								}
								buttons.appendChild(button)
							}
					}

					vps_ssh_command('status')

					document.getElementById('installation_type_buttons').querySelectorAll('.button').forEach(button => button.classList.remove('is-loading'))

					document.getElementById('installation_type_auto_button').onclick = () =>
					{
						localStorage.setItem('installation_type', 'auto')
						document.getElementById('modal_installation_type').classList.remove('is-active')
						auto_install()
					}

					document.getElementById('installation_type_manual_button').onclick = () =>
					{
						localStorage.setItem('installation_type', 'manual')
						document.getElementById('modal_installation_type').classList.remove('is-active')
					}
				}
				else if (output.message?.substring(0, 21) == 'SSH connection failed')
				{
					bulmaToast.toast({ message: 'La connexion au serveur a échoué', type: 'is-danger' })
					document.getElementById('modal_installation_type').classList.remove('is-active')
				}
				else if (output.message == 'SSH authentication failed')
				{
					bulmaToast.toast({ message: 'L\'authentification sur le serveur a échoué', type: 'is-danger' })
					document.getElementById('modal_installation_type').classList.remove('is-active')
				}
				else if (output.message == 'SSH session disconnected')
					vps_ssh_connect()
				else if (output.message == 'Redémarrage...')
				{
					document.getElementById('output').innerHTML += '<div class="has-text-primary">Redémarrage en cours...</div>'
					setTimeout(function ()
					{
						vps_ssh_connect()
						document.getElementById('output').innerHTML += '<div class="has-text-primary">Redémarrage terminé</div>'
						document.getElementById(output.operation + '_button').classList.remove('is-warning')
						document.getElementById(output.operation + '_button').classList.remove('is-danger')
						document.getElementById(output.operation + '_button').classList.remove('is-loading')
						document.getElementById(output.operation + '_button').classList.add('is-primary')
						document.getElementById('modal_log_close').classList.add('is-hidden')
						if (localStorage.getItem('installation_type') == 'auto')
							setTimeout(() => auto_install(), 2000)
					}, 15000)
				}
				else if (output.operation == 'create_admin' && output.progress == 100)
				{
					document.getElementById('continue8').classList.remove('is-loading')
					load(9)
						.then(() => setTimeout(() => vps_ssh_command('get_config'), 100))
				}
				else if (output.operation == 'get_config' && output.progress == 100)
				{
					document.getElementById('config_ip').innerText = output.ip
					document.getElementById('config_ssh_port').innerText = output.ssh_port
					document.getElementById('config_debian_password').innerText = output.debian_password
					if (output.two_fa_key)
					{
						document.getElementById('2fa_block').classList.remove('is-hidden')
						document.getElementById('2fa_listitem').classList.remove('is-hidden')
						document.getElementById('config_2fa_key').innerText = output.two_fa_key
						new QRCode(document.getElementById("qrcode"), "otpauth://totp/debian%40" + output.domain + "?secret=" + output.two_fa_key + "&issuer=OPTIMUS")
					}
					if (output.port_knocking_sequence)
					{
						document.getElementById('port_knocking_listitem').classList.remove('is-hidden')
						document.getElementById('config_port_knocking_sequence').innerText = output.port_knocking_sequence
					}
					document.getElementById('config_zip').innerText = output.zip
				}
				else if (output.operation == 'create_admin' && output.message == 'Le compte administrateur a été créé avec succès')
					load(9)
				else if (document.getElementById('output') && output.operation && output.code == 200)
				{
					let color_class = 'has-text-black'
					if (output.color == 'green')
						color_class = 'has-text-primary'
					else if (output.color == 'red')
						color_class = 'has-text-danger'
					else if (output.color == 'yellow')
						color_class = 'has-text-warning'
					else if (output.color == 'blue')
						color_class = 'has-text-info'

					if (output.message && output.message != 'status' && output.progress != 0)
						document.getElementById('output').innerHTML += '<div class="' + color_class + '">' + output.message + '</div>'
					if (output.code == 200 && document.getElementById(output.operation + '_button'))
					{
						document.getElementById('progress').value = output.progress
						document.getElementById('progress_text').innerText = output.progress + ' %'
						if (output.progress == 0)
						{
							document.getElementById('modal_log').classList.add('is-active')
							document.querySelector('.progress').classList.add('is-loading')
							document.getElementById('modal_log_close').classList.add('is-hidden')
							document.querySelector('#output_header > p').innerText = output.message
							document.querySelector('#output_header > button').classList.add('is-loading')
							document.getElementById(output.operation + '_button').classList.add('is-loading')
						}
						else if (output.progress == 100)
						{
							document.getElementById(output.operation + '_button').classList.add('is-primary')
							document.getElementById(output.operation + '_button').classList.add('is-light')
							document.getElementById(output.operation + '_button').classList.remove('is-warning')
							document.getElementById(output.operation + '_button').classList.remove('is-danger')
							document.getElementById(output.operation + '_button').classList.remove('is-loading')
							document.querySelector('#output_header > button').classList.remove('is-loading')
							if (localStorage.getItem('installation_type') == 'auto' && output.message != 'status')
								setTimeout(() => auto_install(), 2000)
							else
								document.getElementById('modal_log_close').classList.remove('is-hidden')

							document.getElementById('continue7').classList.remove('is-hidden')
							for (module of modules)
								if (module.required == 1 && !document.getElementById(module.id + '_button').classList.contains('is-primary'))
									document.getElementById('continue7').classList.add('is-hidden')
						}
						else
						{
							document.getElementById(output.operation + '_button').classList.remove('is-primary')
							document.getElementById(output.operation + '_button').classList.add('is-outlined')
						}
					}
				}
				else if (document.getElementById('output') && output.operation == 'create_admin' && output.error)
					bulmaToast.toast({ message: output.error, type: 'is-danger' })
				else if (document.getElementById('output') && output.operation && output.code >= 400)
				{
					document.getElementById('output').innerHTML += '<div class="has-text-danger">' + output.message + '</div>'
					document.getElementById(output.operation + '_button').classList.remove('is-primary')
					document.getElementById(output.operation + '_button').classList.add('is-outlined')
					document.getElementById(output.operation + '_button').classList.remove('is-loading')
					document.getElementById('modal_log_close').classList.remove('is-hidden')
				}
				else if (document.getElementById('output') && output.operation && output.code == 200 && output.error)
				{
					document.getElementById('output').innerHTML += '<div class="has-text-danger">' + output.error + '</div>'
					document.getElementById("output").scrollTop = document.getElementById("output").scrollHeight
				}
			})
			socket.addEventListener("open", event =>
			{



			})
			socket.addEventListener("close", event => bulmaToast.toast({ message: 'La connexion au serveur été interrompue', type: 'is-danger' }))
		}
		else
			return false
	}
	else if (id == 10)
	{
		document.getElementById('final_link').innerText = 'https://optimus.' + localStorage.getItem('domain')
		document.getElementById('remember_admin_email').innerText = document.getElementById('admin_email').value
		document.getElementById('remember_admin_password').innerText = document.getElementById('admin_password').value

	}
}

async function validate(step)
{
	if (step == 0)
	{
		if (localStorage.getItem('has_read_cgv') != 'true')
			return false
		localStorage.setItem('has_read_cgv', true)
		document.getElementById('steps').classList.remove('is-hidden')
		document.getElementById('new_install_button').classList.remove('is-hidden')
		load(1)
	}
	else if (step == 1)
	{
		if (localStorage.getItem('has_ovh_account') != 'true')
			return false
		load(2, !localStorage.getItem('app_key'))
	}
	else if (step == 2)
	{
		if (document.getElementById('app_key').value == '')
			bulmaToast.toast({ message: 'Le champ "Application key" doit être renseigné pour continuer', type: 'is-danger' })
		else if (document.getElementById('app_key').value.length != 16)
			bulmaToast.toast({ message: 'Le champ "Application key" doit comporter 16 caractères', type: 'is-danger' })
		else if (document.getElementById('secret_key').value == '')
			bulmaToast.toast({ message: 'Le champ "Secret key" doit être renseigné pour continuer', type: 'is-danger' })
		else if (document.getElementById('secret_key').value.length != 32)
			bulmaToast.toast({ message: 'Le champ "Secret key" doit comporter 32 caractères', type: 'is-danger' })
		else
		{
			localStorage.setItem('app_key', document.getElementById('app_key').value)
			localStorage.setItem('secret_key', document.getElementById('secret_key').value)
			load(3, !localStorage.getItem('consumer_key'))
		}
	}
	else if (step == 3)
	{
		if (document.getElementById('consumer_key').value == '')
			bulmaToast.toast({ message: 'Le champ "Consumer key" doit être renseigné pour continuer', type: 'is-danger' })
		else if (document.getElementById('consumer_key').value.length != 32)
			bulmaToast.toast({ message: 'Le champ "Consumer key" doit comporter 32 caractères', type: 'is-danger' })
		else
		{
			localStorage.setItem('app_key', document.getElementById('app_key').value)
			localStorage.setItem('secret_key', document.getElementById('secret_key').value)
			load(4)
		}
		return false
	}
	else if (step == 4)
	{
		if (!localStorage.getItem('domain'))
			return false
		load(5)
	}
	else if (step == 5)
	{
		if (!localStorage.getItem('vps'))
			return false
		load(6)
	}
	else if (step == 6)
	{
		if (!localStorage.getItem('os'))
			return false
		load(7)
	}
	return false
}


async function generate_admin_credentials()
{
	document.getElementById('admin_domain').innerText = localStorage.getItem('domain')
	document.getElementById('admin_email').value = 'admin@' + localStorage.getItem('domain')
	document.getElementById('admin_password').value = ''
	var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
	for (var i = 0; i < 16; i++)
	{
		var rnum = Math.floor(Math.random() * chars.length)
		document.getElementById('admin_password').value += chars.substring(rnum, rnum + 1)
	}
}


function saveAs(uri, filename)
{
	var link = document.createElement('a')
	if (typeof link.download === 'string')
	{
		link.href = uri
		link.download = filename
		document.body.appendChild(link)
		link.click()
		document.body.removeChild(link)
	}
	else
		window.open(uri)
}


function auto_install()
{
	for (module of modules)
		if (module.autoinstall == true)
			if (!document.getElementById(module.id + '_button').classList.contains('is-primary'))
			{
				if (module.id == 'optimus-installer')
					vps_ssh_command('optimus-installer', { domain: document.getElementById('domain_select').value, app_key: localStorage.getItem('app_key'), consumer_key: localStorage.getItem('consumer_key'), secret_key: localStorage.getItem('secret_key') })
				else
					vps_ssh_command(module.command, module.parameters)
				return false
			}
	document.getElementById('modal_log').classList.remove('is-active')
}


window.onload = async () => 
{
	for (i = 0; i <= 7; i++)
		if (await load(i) == false)
			break
}